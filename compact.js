console.log(Array.from(Array(10), () => new Array(2)).map((_, i) => {
  tiro1 = Math.floor(Math.random() * 11)
  return (tiro1 === 10 && i === 9) ? [10, 0, Math.floor(Math.random() * 11)] :
    [tiro1 , Math.floor(Math.random() * (11 - tiro1))]}).reduce((v, e, i, a) => {
  return (e[0] === 10 && i < 9) ? v + (10 + a[i + 1][0] + a[i + 1][1]) :
    (e[0] === 10 && i === 9) ? v + (10 + e[2]) :
    (e[0] + e[1] === 10) && (i < 9) ?
    v + (e[0] + e[1] + a[i + 1][0]) :
    (e[0] + e[1] === 10) && (i === 9) ? v + (10 + e[2]) :
    v + (e[0] + e[1]) }, 0))
